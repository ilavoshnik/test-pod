# illia-testpod

[![CI Status](https://img.shields.io/travis/ilavash/illia-testpod.svg?style=flat)](https://travis-ci.org/ilavash/illia-testpod)
[![Version](https://img.shields.io/cocoapods/v/illia-testpod.svg?style=flat)](https://cocoapods.org/pods/illia-testpod)
[![License](https://img.shields.io/cocoapods/l/illia-testpod.svg?style=flat)](https://cocoapods.org/pods/illia-testpod)
[![Platform](https://img.shields.io/cocoapods/p/illia-testpod.svg?style=flat)](https://cocoapods.org/pods/illia-testpod)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

illia-testpod is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'illia-testpod'
```

## Author

ilavash, electrohim@yandex.ru

## License

illia-testpod is available under the MIT license. See the LICENSE file for more info.
