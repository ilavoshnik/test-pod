//
//  main.m
//  illia-testpod
//
//  Created by ilavash on 06/04/2018.
//  Copyright (c) 2018 ilavash. All rights reserved.
//

@import UIKit;
#import "ILAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([ILAppDelegate class]));
    }
}
