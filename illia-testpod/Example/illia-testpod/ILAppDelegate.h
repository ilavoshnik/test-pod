//
//  ILAppDelegate.h
//  illia-testpod
//
//  Created by ilavash on 06/04/2018.
//  Copyright (c) 2018 ilavash. All rights reserved.
//

@import UIKit;

@interface ILAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
