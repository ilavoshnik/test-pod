#
# Be sure to run `pod lib lint illia-testpod.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'illia-testpod'
  s.version          = '0.2.3'
  s.summary          = 'Bla bla bla vlas fjs dk jfdskj fksdj fkdsj'
  s.description      = <<-DESC
Lorem ipsum djhsfh shfs jkhf sdkjh dsjkhjh kjdh
                       DESC

  s.homepage         = 'https://gitlab.com/ilavoshnik/test-pod'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = 'fdskjf sdjfkhdsjkfdks '
  s.author           = { 'ilavash' => 'electrohim@yandex.ru' }
  s.source           = { :git => 'https://gitlab.com/ilavoshnik/test-pod.git', :tag => s.version.to_s }

  s.ios.deployment_target = '8.0'

  s.source_files = 'illia-testpod/Classes/**/*'

end
